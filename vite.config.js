// vite.config.js

import { defineConfig } from 'vite';

export default defineConfig({
  build: {
    manifest: true, // This generates a manifest file
  },
});
